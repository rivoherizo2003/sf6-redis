<?php

namespace App\Service;

use Predis\Client;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class RedisService
{
    /**
     * @param array $options
     * @return Client
     */
    public function connectRedisClient(array $options):Client
    {
        return RedisAdapter::createConnection($_ENV['REDIS_DNS'], $options);
//        return new Client($_ENV['REDIS_DNS'], $options);
    }
}