<?php

namespace App\Service;

use Predis\PubSub\AbstractConsumer;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageService
{
    /**
     * @param array $data
     * @param RedisService $redisService
     * @return string
     */
    public function sendMsg(array $data, RedisService $redisService, UserInterface $user): string
    {
        if (key_exists('message', $data)) {
            $client = $redisService->connectRedisClient([]);
            $client->executeRaw(['LPUSH', 'list_msg', "[".$user->getUserIdentifier()."]: ".$data['message']]);

            return 'ok';
        }

        return 'ko';
    }

    /**
     * @param RedisService $redisService
     * @return mixed|string
     */
    public function retrieveMsg(RedisService $redisService): mixed
    {
        $client = $redisService->connectRedisClient(['read_timeout' => 10]);

        return $client->executeRaw(['LRANGE', 'list_msg',0,-1]);
    }
}