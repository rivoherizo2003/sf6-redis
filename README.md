Symfony Demo Application && redis cache
========================

The "Symfony Demo Application" is a reference application created to show how
to develop applications following the [Symfony Best Practices][1].

Requirements
------------

  * docker installed on your pc

Installations of dependencies
------------

Clone the project

```bash
$ git clone https://gitlab.com/rivoherizo2003/sf6-redis.git sf6-redis
```

Move to the current project and launch these commands
```bash
$ docker-compose up -d
```

Install dependencies symfony
```bash
$ docker exec -i sf6_php composer install
```

Create database
```bash
$ docker exec -i sf6_php php bin/console doctrine:database:create
```

Migrate table
```bash
$ docker exec -i sf6_php php bin/console doctrine:migrations:migrate
```

Load fixtures
```bash
$ docker exec -i sf6_php php bin/console doctrine:fixtures:load
```

Install node dependencies
```bash
$ docker exec -i sf6_node yarn install
```

Compile assets
```bash
$ docker exec -i sf6_node yarn run encore dev
```

Url to launch the symfony demo
- http://localhost:9556/

Tests
-----

Execute this command to run tests:

```bash
$ cd my_project/
$ ./bin/phpunit
```

[1]: https://symfony.com/doc/current/best_practices.html
[2]: https://symfony.com/doc/current/setup.html#technical-requirements
[3]: https://symfony.com/doc/current/setup/web_server_configuration.html
[4]: https://symfony.com/download

To delete folder var and data (volume)
---
```bash
 sudo rm -rf data/* && sudo rm -rf var/* && sudo rmdir data && sudo rmdir var
 ```

 Symfony and Redis
 ----------------
 - Store list of articles from doctrine query in cache redis
 - Little tchat using LIST REDIS

