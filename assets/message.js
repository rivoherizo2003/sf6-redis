$(function () {
    const formPubSub = $('#form_pub_sub');
    formPubSub.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).data('path'),
            data: $(this).serialize(),
            success: function () {
                formPubSub.trigger('reset');
                retrieveMessages();
            }
        })
    });
    retrieveMessages();
    setInterval(function (){
        retrieveMessages();
    },10000);

});

retrieveMessages = function () {
    $.ajax({
        type: 'GET',
        url: $('#container-message').data('path'),
        success: function (json) {
            let messages = '';
            let i = 0;
            let msg = json.messages.reverse();
            msg.forEach(function (value, key){
                let classMsg = "alert-success";
                if (i%2 !== 0) classMsg = "alert-warning";
                messages +='<div class="alert '+classMsg+'">' + value + '</div>';
                i++;
            });
            console.log(messages);
            $('#container-message').html(messages);
        }
    });
};